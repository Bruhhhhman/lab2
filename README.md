# Lab2



## Сценарий программы

Сценарий, который используется в этом проекте, связан с созданием пользовательского интерфейса с темной темой и добавлением дополнительной функциональности к элементам пользовательского интерфейса. Предположим, что у нас есть приложение, которое имеет несколько видов окон и несколько тем оформления. Для реализации такого приложения можно использовать паттерн "Абстрактная фабрика". Далее, для использования паттерна "Декоратор", мы можем создать классы-декораторы, которые будут оборачивать объекты окон и добавлять им новые функциональности.

## Абстрактная фабрика
Паттерн Абстрактная фабрика (Abstract Factory) - это порождающий паттерн проектирования, который позволяет создавать семейства связанных объектов без указания конкретных классов.

В этом паттерне создается абстрактный класс фабрики, который определяет методы для создания различных объектов, а каждая конкретная фабрика наследует этот абстрактный класс и реализует его методы для создания конкретных объектов.

## Декоратор
Паттерн Декоратор (Decorator) - это структурный паттерн проектирования, который позволяет динамически добавлять новое поведение или функциональность объектам, не изменяя их исходный код.

При использовании паттерна Декоратор создается класс-декоратор, который оборачивает оригинальный объект и добавляет к нему новое поведение или функциональность. Таким образом, каждый декоратор является оберткой для другого декоратора или оригинального объекта.


## UML диаграмма классов
```plantuml
@startuml

interface IUIFactory {
    +CreateWindow(): IWindow
    +CreateButton(): IButton
}

interface IWindow {
    +Render(): void
}

interface IButton {
    +Click(): void
}

class DarkUIFactory {
    +CreateWindow(): IWindow
    +CreateButton(): IButton
}

class LightUIFactory {
    +CreateWindow(): IWindow
    +CreateButton(): IButton
}

class DarkWindow {
    +Render(): void
}

class LightWindow {
    +Render(): void
}

class DarkButton {
    +Click(): void
}

class LightButton {
    +Click(): void
}

interface IWindowDecorator {
    +Render(): void
    +Decorate(): void
}

class WindowDecorator {
    -window: IWindow
    +WindowDecorator(window: IWindow)
    +Render(): void
}

class ShadowDecorator {
    +ShadowDecorator(window: IWindow)
    +Render(): void
    +Decorate(): void
}

class BorderDecorator {
    +BorderDecorator(window: IWindow)
    +Render(): void
    +Decorate(): void
}

class Client {
    -factory: IUIFactory
    +Client(factory: IUIFactory)
    +RenderUI(): IWindow
}

Program --> IUIFactory
Program --> Client

Client --> IUIFactory
Client --> IWindow
Client --> IButton

DarkUIFactory --> IUIFactory
DarkUIFactory --> IWindow
DarkUIFactory --> IButton

LightUIFactory --> IUIFactory
LightUIFactory --> IWindow
LightUIFactory --> IButton

DarkWindow --> IWindow

LightWindow --> IWindow

DarkButton --> IButton

LightButton --> IButton

IWindowDecorator --> IWindow

WindowDecorator --> IWindowDecorator
WindowDecorator --> IWindow

ShadowDecorator --> IWindowDecorator
ShadowDecorator --> IWindow

BorderDecorator --> IWindowDecorator
BorderDecorator --> IWindow

@enduml
```






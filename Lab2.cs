using System;

namespace Lab2
{
    // Общий интерфейс для фабрик элементов UI
    public interface IUIFactory
    {
        IWindow CreateWindow();
        IButton CreateButton();
    }

    // Конкретная фабрика для создания элементов UI с темной темой
    public class DarkUIFactory : IUIFactory
    {
        public IWindow CreateWindow()
        {
            return new DarkWindow();
        }

        public IButton CreateButton()
        {
            return new DarkButton();
        }
    }

    // Конкретная фабрика для создания элементов UI со светлой темой
    public class LightUIFactory : IUIFactory
    {
        public IWindow CreateWindow()
        {
            return new LightWindow();
        }

        public IButton CreateButton()
        {
            return new LightButton();
        }
    }

    // Интерфейс для элемента UI "окно"
    public interface IWindow
    {
        void Render();
    }

    // Конкретный класс элемента UI "окно" с темной темой
    public class DarkWindow : IWindow
    {
        public void Render()
        {
            Console.WriteLine("Рендеринг окна с темной темой.");
        }
    }

    // Конкретный класс элемента UI "окно" со светлой темой
    public class LightWindow : IWindow
    {
        public void Render()
        {
            Console.WriteLine("Рендеринг окна со светлой темой.");
        }
    }

    // Интерфейс для элемента UI "кнопка"
    public interface IButton
    {
        void Click();
    }

    // Конкретный класс элемента UI "кнопка" с темной темой
    public class DarkButton : IButton
    {
        public void Click()
        {
            Console.WriteLine("Кнопка с темной темой нажата.");
        }
    }

    // Конкретный класс элемента UI "кнопка" со светлой темой
    public class LightButton : IButton
    {
        public void Click()
        {
            Console.WriteLine("Кнопка со светлой темой нажата.");
        }
    }

    // Класс клиента, который использует фабрику для создания элементов UI
    public class Client
    {
        private IUIFactory factory;

        public Client(IUIFactory factory)
        {
            this.factory = factory;
        }

        public IWindow RenderUI()
        {
            IWindow window = factory.CreateWindow();
            IButton button = factory.CreateButton();

            window.Render();
            button.Click();

            return window;
        }
    }

    // Общий интерфейс для декораторов окна
    public interface IWindowDecorator : IWindow
    {
    }

    // Базовый декоратор, который просто запускает метод содержимого окна
    public abstract class WindowDecorator : IWindowDecorator
    {
        protected IWindow window;

        public WindowDecorator(IWindow window)
        {
            this.window = window;
        }

        public virtual void Render()
        {
            window.Render();
        }
    }

    // Конкретный декоратор, который добавляет тень к окну
    public class ShadowDecorator : WindowDecorator
    {
        public ShadowDecorator(IWindow window) : base(window)
        {
        }

        public override void Render()
        {
            base.Render();
            Console.WriteLine("Добавление тени к окну.");
        }
    }

    // Конкретный декоратор, который добавляет границу к окну
    public class BorderDecorator : WindowDecorator
    {
        public BorderDecorator(IWindow window) : base(window)
        {
        }

        public override void Render()
        {
            base.Render();
            Console.WriteLine("Добавление границы к окну.");
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            // Создаем фабрику для элементов UI с темной темой
            IUIFactory darkFactory = new DarkUIFactory();
            // Создаем фабрику для элементов UI со светлой темой
            IUIFactory lightFactory = new LightUIFactory();

            // Создаем клиентский код, который использует фабрику для создания элементов UI
            Client darkClient = new Client(darkFactory);
            Client lightClient = new Client(lightFactory);

            // Создаем окно через клиентский код для темной темы
            IWindow darkWindow = darkClient.RenderUI();

            // Оборачиваем окно в несколько декораторов
            IWindowDecorator decoratedDarkWindow = new BorderDecorator(new ShadowDecorator(darkWindow));

            // Вызываем методы для декорированного окна с темной темой
            decoratedDarkWindow.Render();

            Console.WriteLine();

            // Создаем окно через клиентский код для светлой темы
            IWindow lightWindow = lightClient.RenderUI();

            // Оборачиваем окно в несколько декораторов
            IWindowDecorator decoratedLightWindow = new BorderDecorator(lightWindow);

            // Вызываем методы для декорированного окна со светлой темой
            decoratedLightWindow.Render();

            Console.ReadLine();
        }
    }
}
